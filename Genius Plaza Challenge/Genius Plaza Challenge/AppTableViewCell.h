//
//  AppTableViewCell.h
//  Genius Plaza Challenge
//
//  Created by Padavan on 24/02/2019.
//  Copyright © 2019 Andrey Gibadullin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppTableViewCell : UITableViewCell

- (void)setIndex:(NSInteger)index info:(NSDictionary *)info;

@end

NS_ASSUME_NONNULL_END
