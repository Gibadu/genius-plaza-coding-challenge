//
//  AppTableViewCell.m
//  Genius Plaza Challenge
//
//  Created by Padavan on 24/02/2019.
//  Copyright © 2019 Andrey Gibadullin. All rights reserved.
//

#import "AppTableViewCell.h"

@interface AppTableViewCell ()
@property (nonatomic, strong) UIImageView *artworkImageView;
@property (nonatomic, strong) UILabel *indexLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *kindLabel;

@property (nonatomic, strong) NSDictionary *info;
@end

@implementation AppTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.indexLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.indexLabel];
        self.indexLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.indexLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:0.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.indexLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeLeading multiplier:1.0f constant:20.0f]];
        
        
        self.artworkImageView = [[UIImageView alloc] init];
        self.artworkImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.artworkImageView];
        self.artworkImageView.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artworkImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-10.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artworkImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:10.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artworkImageView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.indexLabel attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:10.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.artworkImageView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.artworkImageView attribute:NSLayoutAttributeHeight multiplier:1.0f constant:0.0f]];
        
        
        self.nameLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.nameLabel];
        self.nameLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.nameLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1.0f constant:10.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.nameLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.artworkImageView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:10.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.nameLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:-10.0f]];
        
        self.kindLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.kindLabel];
        self.kindLabel.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.kindLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1.0f constant:-10.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.kindLabel attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.artworkImageView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:10.0f]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.kindLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationLessThanOrEqual toItem:self.contentView attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:-10.0f]];
    }
    return self;
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.artworkImageView.image = nil;
    self.nameLabel.text = @"";
    self.kindLabel.text = @"";
    self.indexLabel.text = @"";
}

- (void)setIndex:(NSInteger)index info:(NSDictionary *)info {
    self.info = [NSDictionary dictionaryWithDictionary:info];
    self.indexLabel.text = [NSString stringWithFormat:@"%d", index+1];
    self.nameLabel.text = [self.info objectForKey:@"name"];
    self.kindLabel.text = [self.info objectForKey:@"kind"];
    
    NSString *artworkUrlString = [self.info objectForKey:@"artworkUrl100"];
    NSString *artworkId = [self.info objectForKey:@"id"];
    if (artworkUrlString != nil) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:artworkUrlString]];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if ([artworkId isEqualToString:[self.info objectForKey:@"id"]] && !error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *image = [UIImage imageWithData:data];
                    self.artworkImageView.image = image;
                });
            }
        }];
        [task resume];
    }
}

@end
