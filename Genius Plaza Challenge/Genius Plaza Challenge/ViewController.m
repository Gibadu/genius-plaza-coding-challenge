//
//  ViewController.m
//  Genius Plaza Challenge
//
//  Created by Padavan on 24/02/2019.
//  Copyright © 2019 Andrey Gibadullin. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"
#import "AppTableViewCell.h";

const NSString *cellIdentifier = @"EmptyCell";

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *results;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView = [[UITableView alloc] init];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = 80;
    [[self tableView] registerClass:[AppTableViewCell class] forCellReuseIdentifier:NSStringFromClass([AppTableViewCell class])];
    [self.view addSubview:self.tableView];
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1.0f constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeLeading multiplier:1.0f constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:0.0f]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:0.0f]];
    
    [self requestDataFromServer];
}

- (void)requestDataFromServer {
    [[DataManager sharedInstance] requestData:^(NSArray *results) {
        self.results = [NSArray arrayWithArray:results];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
        });
    } onFail:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *message = error.localizedDescription;
            if (message == nil) {
                message = @"Something went wrong";
            }
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Oops!" message:message preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"Okay :(" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Try Again!" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self requestDataFromServer];
            }]];
            [self presentViewController:alert animated:YES completion:NULL];
        });
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AppTableViewCell *cell = (AppTableViewCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AppTableViewCell class])];
    [cell setIndex:indexPath.row info:[self.results objectAtIndex:indexPath.row]];
    return cell;
}

@end
