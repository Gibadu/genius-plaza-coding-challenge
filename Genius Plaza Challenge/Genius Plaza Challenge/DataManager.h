//
//  DataManager.h
//  Genius Plaza Challenge
//
//  Created by Padavan on 24/02/2019.
//  Copyright © 2019 Andrey Gibadullin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DataManager : NSObject
+ (DataManager *)sharedInstance;
- (void)requestData:(void (^)(NSArray *))onSuccess onFail:(void (^)(NSError *))onFail;
@end

NS_ASSUME_NONNULL_END
