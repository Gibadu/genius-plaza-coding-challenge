//
//  DataManager.m
//  Genius Plaza Challenge
//
//  Created by Padavan on 24/02/2019.
//  Copyright © 2019 Andrey Gibadullin. All rights reserved.
//

#import "DataManager.h"

const NSString *apiBasePath = @"https://rss.itunes.apple.com/api/v1/";
const NSString *country = @"us";
const NSString *mediaType = @"ios-apps";
const NSString *feedType = @"new-apps-we-love";
const NSString *genre = @"all";
const long count = 100;
const NSString *explicitType = @"explicit";


@implementation DataManager
+ (DataManager *)sharedInstance {
    static DataManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[DataManager alloc] init];
    });
    return sharedInstance;
}

- (NSString *)requestUrlString {
    return [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%ld/%@.json", apiBasePath, country, mediaType, feedType, genre, count, explicitType];
}

- (void)requestData:(void (^)(NSArray *))onSuccess onFail:(void (^)(NSError *))onFail {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[self requestUrlString]]];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error) {
            NSError *jsonError = nil;
            NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonError];
            NSLog(@"%@", dictionary);
            NSDictionary *feed = [dictionary objectForKey:@"feed"];
            NSArray *results = [feed objectForKey:@"results"];
            if (results == nil) {
                onFail([NSError errorWithDomain:@"myError" code:422 userInfo:@{NSLocalizedDescriptionKey: @"Incorrect Response"}]);
            } else {
                onSuccess(results);
            }
        }
        else {
            onFail(error);
        }
    }];
    [task resume];
}

@end
